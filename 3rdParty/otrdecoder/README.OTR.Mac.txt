Deutsch
=======
Bedienungsanleitung:

* Kommandozeile:
Um diesen Dekoder auszuf��hren m��ssen sie zuerst das Terminal starten.
Dazu k��nnen sie Finder benutzen, um danach zu suchen.
Bei der Kommandozeilenversion kann man sich die m��glichen Parameter mittels
	<Dekoderverzeichnis>/otrdecoder -h
anzeigen lassen. Zum Dekodieren einer .OTRKEY Datei muss folglich
	<Dekoderverzeichnis>/otrdecoder -i <OTRKEY-Datei> -o <Ausgabeverzeichnis> -e <Email> -p <Passwort>
eingegeben werden. Wird das Ausgabeverzeichnis nicht angegeben, wird in das
aktuelle Verzeichnis dekodiert. Mit
	<Dekoderverzeichnis>/otrdecoder -v
kann die Version des Dekoders angezeigt werden. Diese bitte immer in Bugreports
angeben. Das erleichtert die Fehlersuche erheblich.
Alternativ zu den obigen Befehlen k��nnen sie auch zuerst in das Dekoderverzeichnis wechseln:
	   cd <Dekoderverzeichnis>
und dann in den obigen Kommandos <Dekoderverzeichnis> durch . ersetzen.

F��r OTRKEY Dateien im neuen Format bietet diese Version des Dekoders
zus��tzlich ein Downloadmanager und die M��glichkeit, die
OTRKEYs on-the-fly zu schneiden.
Diese beiden Features funktionieren allerdings nur bei den OTRKEYs im neuen Format
und (noch) nicht bei den alten OTRKEYs!!!!

Um eine OTRKEY Datei on-the-fly w��hrend des Downloadvorgangs zu dekodieren, geben
sie einfach statt eines lokalen Dateinamens die URL an, von der sie die OTRKEY
Datei herunterladen wollen. Alle anderen Parameter bleiben dieselben wie bei der
lokalen Dekodierung.
Sollte ihr Download abbrechen, k��nnen sie den Download mit derselben Kommandozeile
fortsetzen. Der Dekoder speichert, welche Teile der OTRKEY Datei er schon geladen
hat und kann dann entsprechend den Download fortsetzen, sofern der Server, von dem
geladen wird, dies unterst��tzt. Dazu wird die teilweise geladene OTRKEY Datei im
Ausgabeverzeichnis gespeichert. Wenn sie die OTRKEY Datei stattdessen in einem anderen
Verzeichnis als dem Ausgabeverzeichnis zwischenspeichern wollen, k��nnen sie mit dem
Parameter -c ein anderes Verzeichnis daf��r angeben.
Achtung!!!: Sollte der Download abbrechen und sie eine Downloadfortsetzung durchf��hren,
wird ihnen ein weiterer Dekodierpunkt abgezogen, da ja bereits angefangen wurde, zu
dekodieren. Leider ist bisher keine M��glichkeit eingebaut, die Dekodierberechtigung
zu cachen.

Wenn sie eine OTRKEY Datei on-the-fly schneiden wollen, geben sie einfach mit dem
Parameter -S den Startzeitpunkt bzw. mit -E den Endzeitpunkt im Format hh:mm:ss[.xxx]
an. Alternativ k��nnen sie auch mit dem Parameter -C eine Cutlist Datei angeben,
entsprechend der dann die OTRKEY Datei geschnitten wird.
Wird statt einer lokalen OTRKEY Datei eine URL angegeben, werden zudem auch nur die
Teile der Datei geladen, die f��r den Schnitt erforderlich sind.

MfG
	Cyberwolf (Linux Decoder Support) <linuxdecoder@onlinetvrecorder.com>

www.onlinetvrecorder.com 
-------------------------------------------------------------------------------
Bekannte Bugs:
- Beim Schneiden von Analogaufnahmen wird der Ton unsychron

-------------------------------------------------------------------------------

English
=======
* commandline:
To start this decoder you first need to start the Terminal. You can use Finder
to find the program.
You can display the possible parameters using
	<decoder directory>/otrdecoder -h
To decode a .OTRKEY file you have to input following command:
   	<decoder directory>/otrdecoder -i <OTRKEY-file> -o <output directory> -e <email> -p <password>
If you don't specify the output directory then the output file will be generated
in the current directory.
With	
	<decoder directory>/otrdecoder -v
you can display the version of the decoder. If you give a bug report, always
provide the version number. This makes it a lot easier to find the problem.
Alternatively to the listed commands you could first change into the decoder
directory:
	cd <decoder directory>
and then subsitute <decoder director> with . in the listed commands above.

For OTRKEY files in the new format this version of the decoder also includes
a download manager and the possibility to cut the OTRKEYs on-the-fly.
Those two features are only working for OTRKEYs in the new format and not (yet)
for the old OTRKEYs!!!!

To decode an OTRKEY file on-the-fly while downloading, you simply specify the URL
from which the OTRKEY should be downloaded instead of the local OTRKEY file name.
All other parameters stay the same as for a local decoding.
If the download aborts, it can be resumed by the same command line as it has been
started with. The decoder saves, which parts of the OTRKEY file it already
downloaded and can thus resume from the corresponding position, if the server,
from which the file ist loaded, supports this. For this the partly loaded OTRKEY
file is saved in the output directory. If you want the decoder to save the OTRKEY
file in another directory, you can specify this directory with the parameter -c.
Attention!!!: If the download aborts and you resume the download, you will be
charged another decoding point, since the decoding already started. Sadly there is
of yet no possible included to cache the decoding authorization.

If you want to cut an OTRKEY on-the-fly, you can simply specify the start time with
the parameter -S respectively the end time with -E in the format hh:mm:ss[.xxx].
Alternatively you can specify a cutlist file with the parameter -C from which the
information which parts to cut is loaded.
If you specify an URL instead of a local OTRKEY file name, only those parts of the
file are loaded, that are needed for the cutting process.

MfG
	Cyberwolf (Linux Decoder Support) <linuxdecoder@onlinetvrecorder.com>

www.onlinetvrecorder.com 

-------------------------------------------------------------------------------
Known Bugs:
- On cutting analog recordings the sound is not in sync with the video

-------------------------------------------------------------------------------
Copyright Notiz:
Dieser Dekoder verwendet die Version 0.5 der FFMPEG Bibliothek:
  http://ffmpeg.org/releases/ffmpeg-0.5.tar.bz2
Konfiguriert wurde die Bibliothek mit den folgenden Optionen:
  --disable-gpl --disable-shared --enable-static --disable-ffmpeg
  --disable-ffplay --disable-ffserver --disable-vhook --disable-avfilter
  --disable-devices --disable-filters --disable-decoders --disable-encoders
  --disable-parsers --disable-muxers --disable-demuxers --enable-muxer=avi
  --enable-demuxer=avi --enable-decoder=mpeg4 --enable-decoder=mp3
  --disable-bsfs --enable-parser=mpeg4video --enable-parser=mpegaudio
  --enable-memalign-hack

Die verwendete FFMPEG Bibliothek steht unter der GNU Lesser General Public
Licence (LGPL) Version 2.1 oder sp��ter:
  http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html

Quelle: http://ffmpeg.org/legal.html

Zur Nutzung mit den OTR Downloadservern wurde die Datei libavformat/http.c angepasst.
Die angepasste Version liegt als otrhttp.c bei.
