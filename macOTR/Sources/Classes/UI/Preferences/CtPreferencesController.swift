//
//  CtPreferencesController.swift
//  macOTR
//
//  Created by Bonk, Thomas on 02.02.17.
//  Copyright © 2017 Thomas Bonk Softwareentwicklung. All rights reserved.
//

import Cocoa
import ExtAppKit

public class CtPreferencesController: NSViewController {


    // MARK: - Initialization
    
    public override func awakeFromNib() {
        enter()
        
        self.representedObject = CtAppDelegate.sharedInstance?.userSettings
    }

}
