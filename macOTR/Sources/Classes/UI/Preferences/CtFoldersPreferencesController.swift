//
//  CtFoldersPreferencesController.swift
//  macOTR
//
//  Created by Thomas Bonk on 05.02.17.
//  Copyright © 2017 Thomas Bonk Softwareentwicklung. All rights reserved.
//

import Cocoa
import ExtAppKit

public class CtFoldersPreferencesController: CtPreferencesController {

    // MARK: - Button Action Handlers

    @IBAction public func selectDownloadFolder(sender: AnyObject?) {
        enter()

        self.chooseFolder { path in

            (self.representedObject as! CtUserSettings).downloadFolder = path
        }
    }

    @IBAction public func selectMoviesFolder(sender: AnyObject?) {
        enter()

        self.chooseFolder { path in

            (self.representedObject as! CtUserSettings).moviesFolder = path
        }
    }


    // MARKS: - Private Methods

    private func chooseFolder(completionHandler: @escaping (String) -> Void) {
        enter()

        let openPanel = NSOpenPanel()

        openPanel.allowsMultipleSelection = false
        openPanel.canChooseDirectories = true
        openPanel.canChooseFiles = false
        openPanel.resolvesAliases = true

        openPanel.begin { button in

            if button == NSFileHandlingPanelOKButton {

                if let url = openPanel.urls.first {

                    completionHandler(url.path)
                }
            }
        }
    }
}
