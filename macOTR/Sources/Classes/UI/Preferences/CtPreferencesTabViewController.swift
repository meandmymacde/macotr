//
//  CtPreferencesTabViewController.swift
//  macOTR
//
//  Created by Thomas Bonk on 04.02.17.
//  Copyright © 2017 Thomas Bonk Softwareentwicklung. All rights reserved.
//

import Cocoa
import ExtAppKit

public class CtPreferencesTabViewController: NSTabViewController {

    // MARK: - Private Properties

    private lazy var _originalSizes = [String : NSSize]()
    private var _window: NSWindow? = nil


    public override func viewDidAppear() {

        super.viewDidAppear()

        self._window = self.view.window
    }


    // MARK: - NSTabViewDelegate

    public override func tabView(_ tabView: NSTabView, willSelect tabViewItem: NSTabViewItem?) {

        enter()

        super.tabView(tabView, willSelect: tabViewItem)

        self._window = self.view.window

        // Cache the size of the tab view.
        if let tabViewItem = tabViewItem, let size = tabViewItem.view?.frame.size {

            if self._originalSizes[tabViewItem.label] == nil {

                self._originalSizes[tabViewItem.label] = size
            }
        }

    }

    public override func tabView(_ tabView: NSTabView, didSelect tabViewItem: NSTabViewItem?) {
        enter()

        super.tabView(tabView, didSelect: tabViewItem)

        if let tabViewItem = tabViewItem {

            self._window?.title = tabViewItem.label
            self.resizeWindowToFit(tabViewItem: tabViewItem)
        }
    }


    // MARK: - Private Methods

    /// Resizes the window so that it fits the content of the tab.
    private func resizeWindowToFit(tabViewItem: NSTabViewItem) {
        enter()

        if let _ = self._window {

            let size = self._originalSizes[tabViewItem.label]

            let contentRect = NSRect(x: 0, y: 0, width: (size?.width)!, height: (size?.height)!)
            let contentFrame = self._window?.frameRect(forContentRect: contentRect)
            let toolbarHeight = (self._window?.frame.size.height)! - (contentFrame?.size.height)!
            let newOrigin = NSPoint(x: (self._window?.frame.origin.x)!, y: (self._window?.frame.origin.y)! + toolbarHeight)
            let newFrame = NSRect(origin: newOrigin, size: (contentFrame?.size)!)

            self._window?.setFrame(newFrame, display: false, animate: true)
        }
    }
}
