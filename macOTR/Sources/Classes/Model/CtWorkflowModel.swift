//
//  CtWorkflowModel.swift
//  macOTR
//
//  Created by Bonk, Thomas on 17.02.17.
//  Copyright © 2017 Thomas Bonk Softwareentwicklung. All rights reserved.
//

import Foundation
import ExtAppKit

public class CtWorkflowModel {
    
    // MARK: - Private Enums
    
    private enum WorkflowStatus: XStateType {
        case Initial
        case DownloadOrDecrypt
        case Download
        case Decrypt
        case DownloadCutlist
        case NoCutlist
        case ParseCutlist
        case CutlistError
        case CutMovie
        case CutMovieError
        case Finished
    }
    
    private enum WorkflowEvents: XEventType {
        case Start
        case StartDownload
        case StartDecrypt
        case StartDownloadCutlist
        case RaiseNoCutlist
        case StartParseCutlist
        case RaiseCutlistError
        case StartCutMovie
        case RaiseCutMovieError
        case RaiseFinished
    }
    
    
    // MARK: - Private Properties
    
    private var _stateMachine = XStateMachine<WorkflowStatus, WorkflowEvents>(initialState: .Initial)
    
    
    // MARK: - Public Properties
    
    public var fileUrl: NSURL! = nil
    
    
    // MARK: - Initialization
    
    public init() throws {
        enter()
        
        try self.initializeStateMachine()
    }
    
    private func initializeStateMachine() throws  {
        enter()
        
        try self._stateMachine += (.Start,                .Initial,           .DownloadOrDecrypt)
        try self._stateMachine += (.StartDownload,        .DownloadOrDecrypt, .Download)
        try self._stateMachine += (.StartDecrypt,         .DownloadOrDecrypt, .Decrypt)
        try self._stateMachine += (.RaiseFinished,        .DownloadOrDecrypt, .Finished)
        try self._stateMachine += (.StartDecrypt,         .Download,          .Decrypt)
        try self._stateMachine += (.StartDownloadCutlist, .Decrypt,           .DownloadCutlist)
        try self._stateMachine += (.RaiseNoCutlist,       .DownloadCutlist,   .NoCutlist)
        try self._stateMachine += (.StartParseCutlist,    .DownloadCutlist,   .ParseCutlist)
        try self._stateMachine += (.RaiseCutlistError,    .ParseCutlist,      .CutlistError)
        try self._stateMachine += (.StartCutMovie,        .ParseCutlist,      .CutMovie)
        try self._stateMachine += (.RaiseCutMovieError,   .CutMovie,          .CutMovieError)
        try self._stateMachine += (.RaiseFinished,        .CutMovie,          .Finished)
        
        (self._stateMachine, .DownloadOrDecrypt) >+ self.checkDownloadOrDecrypt
        
        self._stateMachine.userInfo = self
    }
    
    
    // MARK: - State Handlers
    
    private func checkDownloadOrDecrypt(_ stateMachine: XStateMachine<WorkflowStatus, WorkflowEvents>,
                                        _ event: WorkflowEvents?,
                                        _ fromState: WorkflowStatus,
                                        _ toState: WorkflowStatus,
                                        _ userInfo: Any?) {
        enter()
        
        guard let _ = self.fileUrl else {
            
            try! WorkflowEvents.RaiseFinished => self._stateMachine
            return
        }
        
        if self.fileUrl.isFileURL || self.fileUrl.isFileReferenceURL() {
        
            try! WorkflowEvents.StartDecrypt => self._stateMachine
        }
        else {
            
            try! WorkflowEvents.StartDownload => self._stateMachine
        }
    }
}
