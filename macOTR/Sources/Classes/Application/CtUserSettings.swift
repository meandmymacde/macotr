//
//  CtUserSettings.swift
//  macOTR
//
//  Created by Thomas Bonk on 02.02.17.
//  Copyright © 2017 Thomas Bonk Softwareentwicklung. All rights reserved.
//

import Cocoa
import ExtAppKit
import KeychainAccess

@objc
public class CtUserSettings: XUserSettings {

    // MARK: - Private Enums

    private enum Keys: String {

        case Username                   = "username"
        case Password                   = "password"
        case DownloadFolder             = "downloadFolder"
        case MoviesFolder               = "moviesFolder"
        case VerifyKeyFiles             = "verifyKeyFiles"
        case OverwriteOutputFiles       = "overwriteOutputFiles"
        case CutListServer              = "cutListServer"
        case MinimumAuthorCutListRating = "minimumAuthorCutListRating"
        case MinimumCutListRating       = "minimumCutListRating"
        case RenameCutMovie             = "renameCutMovie"
    }


    // MARK: - Initialization

    public override func awakeFromNib() {
        enter()

        let fm = FileManager()
        let defaults: [String:AnyObject] = [
            Keys.DownloadFolder.rawValue: fm.urls(for: .downloadsDirectory, in: .userDomainMask)[0].path as AnyObject,
            Keys.MoviesFolder.rawValue: fm.urls(for: .moviesDirectory, in: .userDomainMask)[0].path as AnyObject,
            Keys.VerifyKeyFiles.rawValue: true as AnyObject,
            Keys.OverwriteOutputFiles.rawValue: true as AnyObject,
            Keys.CutListServer.rawValue: "http://www.cutlist.at" as AnyObject,
            Keys.MinimumAuthorCutListRating.rawValue: Int(3) as AnyObject,
            Keys.MinimumCutListRating.rawValue: Int(3) as AnyObject,
            Keys.RenameCutMovie.rawValue: true as AnyObject
        ]

        super.registerDefaults(defaults)
    }

    
    // MARK: - Public Properties
    
    public var username: String {
        get {
            enter()

            let value = self.get(.Username, defaultValue: "")
            return value
        }
        set {
            enter()

            self.set(.Username, value: newValue)
        }
    }
    public var password: String {
        get {
            enter()

            let keychain = Keychain(service: "macOTR")

            guard let pwd = keychain[self.username] else {
                return ""
            }

            return pwd
        }
        set {
            enter()

            self.willChangeValue(forKey: Keys.Password.rawValue)
            let keychain = Keychain(service: "macOTR")
            keychain[self.username] = newValue
            self.didChangeValue(forKey: Keys.Password.rawValue)
        }
    }

    public var downloadFolder: String {
        get {
            enter()

            let value = self.get(.DownloadFolder, defaultValue: "")
            return value
        }
        set {
            enter()

            self.set(.DownloadFolder, value: newValue)
        }
    }

    public var moviesFolder: String {
        get {
            enter()

            let value = self.get(.MoviesFolder, defaultValue: "")
            return value
        }
        set {
            enter()

            self.set(.MoviesFolder, value: newValue)
        }
    }

    public var verifyKeyFiles: Bool {
        get {
            enter()

            let value = self.get(.VerifyKeyFiles, defaultValue: true)
            return value
        }
        set {
            enter()

            self.set(.VerifyKeyFiles, value: newValue)
        }
    }

    public var overwriteOutputFiles: Bool {
        get {
            enter()

            let value = self.get(.OverwriteOutputFiles, defaultValue: true)
            return value
        }
        set {
            enter()

            self.set(.OverwriteOutputFiles, value: newValue)
        }
    }

    public var cutListServer: String {
        get {
            enter()

            let value = self.get(.CutListServer, defaultValue: "")
            return value
        }
        set {
            enter()

            self.set(.CutListServer, value: newValue)
        }
    }

    public var minimumAuthorCutListRating: Int {
        get {
            enter()

            let value = self.get(.MinimumAuthorCutListRating, defaultValue: 3)
            return value
        }
        set {
            enter()

            self.set(.MinimumAuthorCutListRating, value: newValue)
        }
    }

    public var minimumCutListRating: Int {
        get {
            enter()

            let value = self.get(.MinimumCutListRating, defaultValue: 3)
            return value
        }
        set {
            enter()

            self.set(.MinimumCutListRating, value: newValue)
        }
    }

    public var renameCutMovie: Bool {
        get {
            enter()

            let value = self.get(.RenameCutMovie, defaultValue: false)
            return value
        }
        set {
            enter()

            self.set(.RenameCutMovie, value: newValue)
        }
    }


    // MARK: - Setting and getting values

    private func get<T>(_ key: Keys, defaultValue: T) -> T {
        enter()

        guard let value = super.getUserDefault(key.rawValue) else {

            return defaultValue
        }

        return value as! T
    }

    private func set<T>(_ key: Keys, value: T) {
        enter()

        self.willChangeValue(forKey: key.rawValue)
        super.setUserDefault(key.rawValue, value: value as AnyObject)
        self.didChangeValue(forKey: key.rawValue)
    }
}
