//
//  CtAppDelegate.swift
//  macOTR
//
//  Created by Thomas Bonk on 01.02.17.
//  Copyright © 2017 Thomas Bonk Softwareentwicklung. All rights reserved.
//

import Cocoa

@NSApplicationMain
public class CtAppDelegate: NSObject, NSApplicationDelegate {
    
    // MARK: - Public Class Properties
    
    public static let sharedInstance: CtAppDelegate? = { NSApplication.shared().delegate as! CtAppDelegate }()
    
    
    // MARK: - Public Properties:
    
    @IBOutlet public private(set) var userSettings: CtUserSettings! = nil
    

    // MARK: - NSApplicationDelegate

    public func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    public func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

